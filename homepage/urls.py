from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path("", views.home, name='home'),
    path("about/", views.about, name='about'),
    path("resume/", views.resume, name='resume'),
    path("galeri/", views.galeri, name='galeri')
]